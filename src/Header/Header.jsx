import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

export default class Header extends Component {
  render() {
    return (
      <div>
        <NavLink className={"btn btn-warning"} to ="/">Phana</NavLink>
        <NavLink className={"btn btn-warning mx-5"} to ="/login">Phanb</NavLink>
        <NavLink className={"btn btn-warning"} to ="/detail">Phanc</NavLink>
      </div>
    )
  }
}
