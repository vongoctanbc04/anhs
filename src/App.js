import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route } from "react-router-dom";
import Phana from "./Pages/Phana/Phana";
import Phanb from "./Pages/Phanb/Phanb";
import Phanc from "./Pages/Phanc/Phanc";
import Trangnhap from "./Pages/Trangnhap";

function App() {
  return (
    <div className="container">
      {/* cái này thì bình thường nè anh */}
      {/* <Trangnhap/> */}

      {/* cái này bị lỗi anh xem dùmm em với nội dung trong từng cái
  nó không hiện rõ á á ví dụ: trong cái phần phana.js 
  có nội dung là h1 là hello thì nó không hiện lênn đuọc được á ah */}
      <BrowserRouter>
        <Route path="/" exact component={Phana} />
        <Route path="/login" component={Phanb} />
        <Route path="/detail" component={Phanc} />
      </BrowserRouter>
    </div>
  );
}

export default App;
